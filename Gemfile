if RUBY_VERSION =~ /1.9/
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8
end

source 'https://rubygems.org'

gem 'rails', '>=6.0.2'

gem 'ey_config'
gem 'rails_autolink'
gem 'simple_form'


# Assets
gem 'turbolinks'
gem 'jquery-rails'
gem 'sass-rails'
gem 'coffee-rails'
gem 'uglifier'

gem 'devise'
gem 'cancancan'
gem 'devise-two-factor'
gem "attr_encrypted", "~> 3.1.0"


gem 'mini_magick'


gem 'geocoder', '>=1.6.1'

gem 'browser'


platform :ruby do
  gem 'newrelic_rpm'
  gem 'unicorn'
  gem 'json'
  gem 'minitest'
  gem 'psych'
  gem 'racc'
end

# Use Puma as the app server
gem 'puma', '~> 3.11'

gem 'pg', '>= 0.18', '< 2.0'

platform :mswin, :mingw, :x64_mingw do
  gem 'tzinfo-data'
end

platforms :jruby do
  ar_jdbc_version = '~> 1.3'
  gem 'activerecord-jdbc-adapter', ar_jdbc_version
  gem 'activerecord-jdbcpostgresql-adapter', ar_jdbc_version
  gem 'jdbc-postgres', :require => false
  gem 'jruby-openssl'
  gem 'trinidad'
end

platform :rbx do
  gem 'rubysl'
  gem 'rubysl-test-unit', :require => false
end

# Bundle gems for the local environment. Make sure to
# put test-only gems in this group so their generators
# and rake tasks are available in development mode:
group :development, :test do
  #gem 'rack-mini-profiler'
  #gem 'flamegraph'
  #gem 'stackprof' # ruby 2.1+ only
  gem 'memory_profiler'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'faker'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'bullet'
  gem 'rack-mini-profiler', require: false
end

gem 'redis'

gem 'sidekiq'
gem 'sidekiq-cron'


# pagination tools
gem 'kaminari'

#
gem 'highline'

gem 'ffi'