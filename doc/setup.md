

Run the following seed 

```
rake db:seed:countries
rake db:seed:cities
rake db:seed:doctor_profiles_statues
rake db:seed:roles
rake db:seed:specialities
rake db:seed:appointments_statues
```