# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_21_105304) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "appointment_logs", force: :cascade do |t|
    t.jsonb "details"
    t.bigint "appointment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["appointment_id"], name: "index_appointment_logs_on_appointment_id"
  end

  create_table "appointment_statuses", force: :cascade do |t|
    t.integer "status"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "appointments", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "professional_id"
    t.bigint "professional_availability_id"
    t.bigint "state_id"
    t.date "date"
    t.integer "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "time"
    t.index ["client_id"], name: "index_appointments_on_client_id"
    t.index ["professional_availability_id"], name: "index_appointments_on_professional_availability_id"
    t.index ["professional_id"], name: "index_appointments_on_professional_id"
    t.index ["state_id"], name: "index_appointments_on_state_id"
  end

  create_table "cities", force: :cascade do |t|
    t.bigint "country_id"
    t.string "code"
    t.string "name"
    t.string "name_ar"
    t.string "name_fr"
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_cities_on_country_id"
    t.index ["name", "name_fr", "name_ar"], name: "index_cities_on_name_and_name_fr_and_name_ar"
    t.index ["name"], name: "index_cities_on_name"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "body"
    t.string "organisation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conversation_messages", force: :cascade do |t|
    t.text "body"
    t.bigint "sender_id"
    t.bigint "conversation_id", null: false
    t.boolean "seen", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conversation_id"], name: "index_conversation_messages_on_conversation_id"
    t.index ["sender_id"], name: "index_conversation_messages_on_sender_id"
  end

  create_table "conversations", force: :cascade do |t|
    t.integer "initializer_id", null: false
    t.integer "participant_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["initializer_id", "participant_id"], name: "index_conversations_on_initializer_id_and_participant_id", unique: true
    t.index ["initializer_id"], name: "index_conversations_on_initializer_id"
    t.index ["participant_id"], name: "index_conversations_on_participant_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "iso_code"
    t.string "name_ar"
    t.string "name_fr"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doctor_availabilities", force: :cascade do |t|
    t.bigint "professional_id"
    t.integer "period"
    t.integer "day"
    t.time "start_from"
    t.time "end_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "full_address"
    t.string "street"
    t.integer "zip_code"
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.bigint "city_id"
    t.bigint "country_id"
    t.index ["city_id"], name: "index_doctor_availabilities_on_city_id"
    t.index ["country_id"], name: "index_doctor_availabilities_on_country_id"
    t.index ["latitude", "longitude"], name: "index_doctor_availabilities_on_latitude_and_longitude"
    t.index ["professional_id"], name: "index_doctor_availabilities_on_professional_id"
  end

  create_table "doctor_profile_statuses", force: :cascade do |t|
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doctor_profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "is_confirmed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "full_address"
    t.string "street"
    t.integer "zip_code"
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.bigint "city_id"
    t.bigint "country_id"
    t.integer "confirmation_status_id"
    t.index ["city_id"], name: "index_doctor_profiles_on_city_id"
    t.index ["country_id"], name: "index_doctor_profiles_on_country_id"
    t.index ["latitude", "longitude"], name: "index_doctor_profiles_on_latitude_and_longitude"
    t.index ["user_id"], name: "index_doctor_profiles_on_user_id", unique: true
  end

  create_table "doctor_speciality_estimation_times", force: :cascade do |t|
    t.bigint "professional_id"
    t.bigint "speciality_id"
    t.integer "time", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["professional_id"], name: "index_doctor_speciality_estimation_times_on_professional_id"
    t.index ["speciality_id"], name: "index_doctor_speciality_estimation_times_on_speciality_id"
  end

  create_table "doctor_votes", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "doctor_id"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_doctor_votes_on_doctor_id"
    t.index ["user_id"], name: "index_doctor_votes_on_user_id"
  end

  create_table "doctors_specialities", force: :cascade do |t|
    t.bigint "doctor_id"
    t.bigint "speciality_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_doctors_specialities_on_doctor_id"
    t.index ["speciality_id"], name: "index_doctors_specialities_on_speciality_id"
  end

  create_table "feature_requests", force: :cascade do |t|
    t.string "sender_name"
    t.string "sender_email"
    t.string "feature"
    t.string "feature_details"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "professional_block_account_histories", force: :cascade do |t|
    t.bigint "professional_id"
    t.string "blocking_reason"
    t.bigint "blocked_by_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blocked_by_id"], name: "index_professional_block_account_histories_on_blocked_by_id"
    t.index ["professional_id"], name: "index_professional_block_account_histories_on_professional_id"
  end

  create_table "professional_configs", force: :cascade do |t|
    t.bigint "professional_id"
    t.integer "slot_consultation_duration_min", default: 10
    t.integer "max_days_from_today_to_be_booked", default: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["professional_id"], name: "index_professional_configs_on_professional_id"
  end

  create_table "professional_holidays", force: :cascade do |t|
    t.bigint "professional_id"
    t.datetime "start_at", null: false
    t.datetime "end_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["end_at"], name: "index_professional_holidays_on_end_at"
    t.index ["professional_id"], name: "index_professional_holidays_on_professional_id"
    t.index ["start_at"], name: "index_professional_holidays_on_start_at"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "spatial_ref_sys", primary_key: "srid", id: :integer, default: nil, force: :cascade do |t|
    t.string "auth_name", limit: 256
    t.integer "auth_srid"
    t.string "srtext", limit: 2048
    t.string "proj4text", limit: 2048
  end

  create_table "specialities", force: :cascade do |t|
    t.string "en_fr"
    t.string "en_ar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["en_ar"], name: "index_specialities_on_en_ar"
    t.index ["en_fr"], name: "index_specialities_on_en_fr"
  end

  create_table "user_profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "is_confirmed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "last_name"
    t.string "gender"
    t.string "title"
    t.date "birthday"
    t.string "address"
    t.integer "zip_code"
    t.index ["user_id"], name: "index_user_profiles_on_user_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "role_id"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer "total_votes_given_by_me", default: 0
    t.integer "votes_to_me_count", default: 0
    t.string "encrypted_otp_secret"
    t.string "encrypted_otp_secret_iv"
    t.string "encrypted_otp_secret_salt"
    t.integer "consumed_timestep"
    t.boolean "otp_required_for_login"
    t.string "two_factor_via", default: "SMS"
    t.boolean "two_factor_via_confirmed", default: false
    t.string "two_factor_via_confirmation_token"
    t.datetime "two_factor_via_confirmation_expires"
    t.string "encrypted_phone_number"
    t.string "encrypted_phone_number_iv"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["encrypted_phone_number_iv"], name: "index_users_on_encrypted_phone_number_iv", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  create_table "whitelist_ip_monitoring_addresses", force: :cascade do |t|
    t.string "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "appointment_logs", "appointments"
  add_foreign_key "appointments", "appointment_statuses", column: "state_id"
  add_foreign_key "appointments", "doctor_availabilities", column: "professional_availability_id"
  add_foreign_key "appointments", "users", column: "client_id"
  add_foreign_key "appointments", "users", column: "professional_id"
  add_foreign_key "conversation_messages", "conversations"
  add_foreign_key "conversation_messages", "users", column: "sender_id"
  add_foreign_key "doctor_availabilities", "users", column: "professional_id"
  add_foreign_key "doctor_profiles", "users"
  add_foreign_key "doctor_speciality_estimation_times", "specialities"
  add_foreign_key "doctor_speciality_estimation_times", "users", column: "professional_id"
  add_foreign_key "doctor_votes", "users"
  add_foreign_key "doctor_votes", "users", column: "doctor_id"
  add_foreign_key "professional_block_account_histories", "users", column: "blocked_by_id"
  add_foreign_key "professional_block_account_histories", "users", column: "professional_id"
  add_foreign_key "professional_configs", "users", column: "professional_id"
  add_foreign_key "professional_holidays", "users", column: "professional_id"
  add_foreign_key "user_profiles", "users"
  add_foreign_key "users", "roles"
end
