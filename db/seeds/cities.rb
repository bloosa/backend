puts 'Running cites'
cities = File.read(Rails.root.join('lib', 'seeds', 'tn.cities.csv'))
cities = CSV.parse(cities, :headers => false, :encoding => 'utf-8')
cities.each do |row|
  name = row[1]
  t = City.find_or_create_by(name: name)
  t.name = name
  t.country_id = Country.where('lower(code) = ?', row[0].downcase).first.id
  t.latitude = row[5]
  t.longitude = row[6]
  t.save
  puts "#{t.name}, #{t.latitude}, #{t.longitude} saved"
end