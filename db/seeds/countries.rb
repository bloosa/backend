

require 'csv'

countries = File.read(Rails.root.join('lib', 'seeds', 'countries.csv'))
countries = CSV.parse(countries, :headers => false, :encoding => 'utf-8')

countries.each do |row|
  t = Country.new
  t.name = row[0]
  t.code = row[1]
  t.iso_code = row[2]
  t.save
  puts "#{t.name}, #{t.iso_code} saved"
end

