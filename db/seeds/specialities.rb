
"Cardiologie#Cardiovasculaire#Chirurgie Carcinologie#Chirurgie Generale#Chirurgie Infantile#Chirurgie Maxillo-faciale#Chirurgie Orthopedique#Chirurgie Plastique#Dermatologie#Endocrinologie#Gastro-hepathologie#Gynecologie-obstetrique#Hematologie#Maladies Infectieuses#Medecine Generale#Medecine Interne#Medecine Legale#Medecine Nucleaire#Medecine Physique#Neurochirurgie#Neurologie#Nutrition-dietetique#Ophtalmologie#Orl Et Stomatologie#Parasitologie#Pediatrie#Pneumologie#Psychiatrie#Radiologie#Rhumatologie#Urologie#Psychologue".split('#').each do |speciality|
  Speciality.find_or_create_by({en_fr: speciality})
end
