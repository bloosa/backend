# Allow to run seed like this rake db:seed:<FILE_NAME>
Dir[File.join(Rails.root, 'db', 'seeds/*', '*.rb')].sort.each do |seed|
  load seed
end