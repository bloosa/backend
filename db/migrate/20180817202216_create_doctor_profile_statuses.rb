class CreateDoctorProfileStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :doctor_profile_statuses do |t|
      t.string :status, unique: true

      t.timestamps null: false
    end
  end
end
