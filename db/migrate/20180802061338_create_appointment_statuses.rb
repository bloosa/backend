class CreateAppointmentStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :appointment_statuses do |t|
      t.integer :status
      t.string :name

      t.timestamps null: false
    end
  end
end
