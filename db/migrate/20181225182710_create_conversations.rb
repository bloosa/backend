class CreateConversations < ActiveRecord::Migration[5.2]
  def change
    create_table :conversations do |t|
      t.integer :initializer_id,     null: false
      t.integer :participant_id, null: false

      t.timestamps
    end
    add_index :conversations, :initializer_id
    add_index :conversations, :participant_id
    add_index :conversations, [:initializer_id, :participant_id], unique: true
  end
end
