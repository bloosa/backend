class CreateSpecialities < ActiveRecord::Migration[5.2]
  def change
    create_table :specialities do |t|
      t.string :en_fr, :unique => true
      t.string :en_ar
      t.timestamps null: false
    end
    add_index :specialities, :en_fr
    add_index :specialities, :en_ar
  end
end
