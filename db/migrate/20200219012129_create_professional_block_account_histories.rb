class CreateProfessionalBlockAccountHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :professional_block_account_histories do |t|
      t.references :professional,  foreign_key: {to_table: :users}
      t.string :blocking_reason
      t.references :blocked_by,  foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
