class CreateProfessionalHolidays < ActiveRecord::Migration[5.2]
  def change
    create_table :professional_holidays do |t|
      t.references :professional, index: true, foreign_key: {to_table: :users}
      t.datetime :start_at, index: true,  null: false
      t.datetime :end_at, index: true, null: false
      t.timestamps
    end
  end
end
