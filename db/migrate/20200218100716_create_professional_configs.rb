class CreateProfessionalConfigs < ActiveRecord::Migration[5.2]
  def change
    create_table :professional_configs do |t|
      t.references :professional, index: true, unique: true, foreign_key: {to_table: :users}
      t.integer :slot_consultation_duration_min, default: 10
      t.integer :max_days_from_today_to_be_booked, default: 30

      t.timestamps
    end
  end
end
