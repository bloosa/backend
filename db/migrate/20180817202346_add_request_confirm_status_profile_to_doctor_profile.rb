class AddRequestConfirmStatusProfileToDoctorProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :doctor_profiles, :confirmation_status, :string, default: "IDLE"
  end
end
