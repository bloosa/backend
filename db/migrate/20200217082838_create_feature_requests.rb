class CreateFeatureRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :feature_requests do |t|
      t.string :sender_name
      t.string :sender_email
      t.string :feature
      t.string :feature_details

      t.timestamps
    end
  end
end
