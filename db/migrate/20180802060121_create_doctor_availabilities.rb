class CreateDoctorAvailabilities < ActiveRecord::Migration[5.2]
  def change
    create_table :doctor_availabilities do |t|
      t.references :professional
      t.integer :period
      t.integer :day
      t.time :start_from
      t.time :end_at

      t.timestamps null: false
    end
    add_foreign_key :doctor_availabilities, :users, column: :professional_id
  end
end
