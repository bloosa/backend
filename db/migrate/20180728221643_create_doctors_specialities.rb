class CreateDoctorsSpecialities < ActiveRecord::Migration[5.2]
  def change
    create_table :doctors_specialities do |t|
      t.references :doctor
      t.references :speciality

      t.timestamps null: false
    end
  end
end
