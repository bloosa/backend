class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.string :code
      t.string :name
      t.string :iso_code
      t.string :name_ar
      t.string :name_fr
      t.float :latitude
      t.float :longitude
      t.timestamps
    end
  end
end
