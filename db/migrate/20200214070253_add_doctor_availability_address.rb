class AddDoctorAvailabilityAddress < ActiveRecord::Migration[5.2]
  def change
    add_column :doctor_availabilities, :full_address, :string
    add_column :doctor_availabilities, :street, :string
    add_column :doctor_availabilities, :zip_code, :integer
    add_column :doctor_availabilities, :latitude, :decimal, {:precision => 10, :scale => 6}
    add_column :doctor_availabilities, :longitude, :decimal, {:precision => 10, :scale => 6}

    add_reference :doctor_availabilities, :city, index: true
    add_reference :doctor_availabilities, :country, index: true

    add_index :doctor_availabilities, [:latitude, :longitude]
  end
end
