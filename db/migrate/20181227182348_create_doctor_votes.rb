class CreateDoctorVotes < ActiveRecord::Migration[5.2]
  def change

    add_column :users, :total_votes_given_by_me, :integer, default: 0

    add_column :users, :votes_to_me_count, :integer, default: 0

    create_table :doctor_votes do |t|
      t.references :user, index: true, foreign_key: true
      t.references :doctor, index: true, foreign_key: {to_table: :users}
      t.integer :value
      # FIXME, add unique combination of user, doctor ,
      # add index
      t.timestamps
    end
  end
end
