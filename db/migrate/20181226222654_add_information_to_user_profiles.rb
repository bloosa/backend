class AddInformationToUserProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :user_profiles, :name, :string
    add_column :user_profiles, :last_name, :string
    add_column :user_profiles, :gender, :string
    add_column :user_profiles, :title, :string
    add_column :user_profiles, :birthday, :date
    add_column :user_profiles, :address, :string
    add_column :user_profiles, :zip_code, :integer
  end
end
