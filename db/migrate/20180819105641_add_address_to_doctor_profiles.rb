class AddAddressToDoctorProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :doctor_profiles, :full_address, :string
    add_column :doctor_profiles, :street, :string
    add_column :doctor_profiles, :zip_code, :integer
    add_column :doctor_profiles, :latitude, :decimal, {:precision => 10, :scale => 6}
    add_column :doctor_profiles, :longitude, :decimal, {:precision => 10, :scale => 6}

    add_reference :doctor_profiles, :city, index: true
    add_reference :doctor_profiles, :country, index: true

    add_index :doctor_profiles, [:latitude, :longitude]
  end
end
