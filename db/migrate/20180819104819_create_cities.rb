class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.references :country
      t.string :code
      t.string :name, index: true
      t.string :name_ar
      t.string :name_fr
      t.decimal :latitude, {:precision => 10, :scale => 6}
      t.decimal :longitude, {:precision => 10, :scale => 6}
      t.timestamps
    end

    add_index :cities, [:name, :name_fr, :name_ar]
  end
end
