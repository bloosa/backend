class AddRoleIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :role, index: true
    add_foreign_key :users, :roles
  end
end
