class CreateWhitelistIpMonitoringAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :whitelist_ip_monitoring_addresses do |t|
      t.string :ip

      t.timestamps
    end
  end
end
