class CreateConversationMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :conversation_messages do |t|
      t.text :body
      t.references :sender, index: true, foreign_key: {to_table: :users}
      t.belongs_to :conversation, foreign_key: true, null: false
      t.boolean :seen, default: false

      t.timestamps
    end

  end
end
