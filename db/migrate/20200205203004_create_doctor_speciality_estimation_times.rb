class CreateDoctorSpecialityEstimationTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :doctor_speciality_estimation_times do |t|
      t.references :professional, index: true, foreign_key: {to_table: :users}
      t.references :speciality, foreign_key: true
      t.integer :time, null: false

      t.timestamps
    end
  end

end
