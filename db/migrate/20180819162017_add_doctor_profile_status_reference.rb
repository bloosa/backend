class AddDoctorProfileStatusReference < ActiveRecord::Migration[5.2]
  def change
    remove_column :doctor_profiles, :confirmation_status, :string
    add_column :doctor_profiles, :confirmation_status_id, :integer, references: :doctor_profile_statuses
  end
end
