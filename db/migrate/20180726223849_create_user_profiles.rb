class CreateUserProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :user_profiles do |t|
      t.belongs_to :user, index: {unique: true}, foreign_key: true
      t.boolean :is_confirmed, default: false
      t.timestamps null: false
    end
  end
end
