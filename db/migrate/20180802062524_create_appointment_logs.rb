class CreateAppointmentLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :appointment_logs do |t|
      t.jsonb :details
      t.references :appointment, index: true

      t.timestamps null: false
    end
    add_foreign_key :appointment_logs, :appointments
  end
end
