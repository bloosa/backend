class AddDeviseTwoFactorToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :encrypted_otp_secret, :string
    add_column :users, :encrypted_otp_secret_iv, :string
    add_column :users, :encrypted_otp_secret_salt, :string
    add_column :users, :consumed_timestep, :integer
    add_column :users, :otp_required_for_login, :boolean


    # two factor via (sms, email, ......)
    add_column :users, :two_factor_via, :string, default: 'SMS'
    add_column :users, :two_factor_via_confirmed, :boolean, default: false
    add_column :users, :two_factor_via_confirmation_token, :string
    add_column :users, :two_factor_via_confirmation_expires, :timestamp


    # email (sms, email, ......)
    add_column :users, :encrypted_phone_number, :string, unique: true
    add_column :users, :encrypted_phone_number_iv, :string, unique: true


    add_index :users, :encrypted_phone_number_iv, unique: true
  end
end
