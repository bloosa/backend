class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.references :client, index: true
      t.references :professional, index: true
      t.references :professional_availability, index: true
      t.references :state, index: true
      t.date :date
      t.integer :price

      t.timestamps null: false
    end
    add_foreign_key :appointments, :users, column: :professional_id
    add_foreign_key :appointments, :users, column: :client_id
    add_foreign_key :appointments, :doctor_availabilities, column: :professional_availability_id
    add_foreign_key :appointments, :appointment_statuses, column: :state_id
  end
end
