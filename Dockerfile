FROM ruby:2.6.6

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
# Set an environment variable where the Rails app is installed to inside of Docker image
ENV RAILS_ROOT /var/www/bloosa

RUN mkdir -p $RAILS_ROOT
# Set working directory
WORKDIR $RAILS_ROOT
# Setting env up

# Adding gems
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN gem install bundler:2.1.4
# https://stephencodes.com/upgrading-ruby-dockerfiles-to-use-bundler-2-0-1/
ENV BUNDLER_VERSION  2.1.4
#set the version in Gemfile
# RUN bundle install --jobs 20 --retry 5 --without development test
RUN bundle config set without 'development test'
RUN bundle install --jobs 20 --retry 5 # --without development test
# Adding project files
COPY . .

# Set all necessary keys for the db:migrate tasks
ARG SSL_CERT_FILE
ARG RAILS_ENV
ARG BLOOSA_SECRET_KEY_BASE
ARG BLOOSA_DATABASE_URI
ARG BLOOSA_SMTP_URI
ARG BLOOSA_SMTP_AUTH_METHOD
ARG BLOOSA_ADMIN_EMAIL
ARG BLOOSA_CONTACT_EMAIL
ARG BLOOSA_PHONE_NUMBER_ENCRYPTION_KEY


# Set env docker 1
ENV DOCKER 1


RUN bundle exec rake db:migrate

# TODO, check these seeds
#RUN bundle exec rake db:seed:countries
#RUN bundle exec rake db:seed:cities
#RUN bundle exec rake db:seed:doctor_profiles_statues
#RUN bundle exec rake db:seed:roles
#RUN bundle exec rake db:seed:specialities
#RUN bundle exec rake db:seed:appointments_statues


RUN bundle exec rake assets:precompile





EXPOSE 3000

CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]