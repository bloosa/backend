redis_uri = "#{ENV['BLOOSA_REDIS_URI']}"
Sidekiq.configure_server do |config|
  config.redis = {url: redis_uri}
end

Sidekiq.configure_client do |config|
  config.redis = {url: redis_uri}
end