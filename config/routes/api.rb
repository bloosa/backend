namespace 'api', module: :api, constraints: {format: :json} do
  get '/', to: 'base#index'
  namespace :v1 do
    match 'professionals/search', to: 'professionals#search', via: [:get]
    match 'professionals/recommended', to: 'professionals#recommended', via: [:get]
    match 'professionals/:id', to: 'professionals#profile', via: [:get]
    match 'specialities/', to: 'specialities#index', via: [:get]
    match "geo/cities", to: 'geo#cities', via: [:get]
    match "geo/", to: 'geo#index', via: [:get]


    namespace :mobile do
      # accounts
      scope :accounts, as: 'accounts' do
        get '/me', to: 'accounts#account'
        post '/sign-up', to: 'accounts#sign_up'
        post '/login', to: 'accounts#login'
        post '/forgot-password', to: 'accounts#forgot_password'
        post '/deactivate', to: 'accounts#deactivate'
      end

      scope :profile, as: 'profile' do
        get '/', to: 'profile#index'
      end

      # accounts
      namespace :appointments do
        post '/new', to: 'appointments#sign_up'
      end

    end

    authenticate :user do
      #message
      match "messages/", to: 'messages#create', via: [:post]

      namespace :professional do
        match "geo/reverse", to: 'geo#reverse', via: [:get]
      end
    end

  end
end