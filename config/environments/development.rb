Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false
  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug
  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true
    config.action_controller.enable_fragment_cache_logging = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options).
  config.active_storage.service = :local

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = false

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true


  config.cache_store = :redis_cache_store

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  if ENV.key?('BLOOSA_SMTP_URI')
    smtp_uri = URI(ENV['BLOOSA_SMTP_URI'])
  else
    smtp_uri = URI('smtp://fake:fake@fake:90')
  end
  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  #
  #
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      address: smtp_uri.host,
      port: smtp_uri.port,
      authentication: ENV['BLOOSA_SMTP_AUTH_METHOD'],
      user_name: CGI.unescape(smtp_uri.user),
      password: CGI.unescape(smtp_uri.password),
      enable_starttls_auto: true
  }

  config.action_mailer.default_url_options = {host: 'localhost', port: '3001'}


  config.active_storage.service = :dev


  config.x.app_mail_config = {
      :admin_mail => ENV['BLOOSA_ADMIN_EMAIL'],
      :contact_mail => ENV['BLOOSA_CONTACT_EMAIL']
  }
end
