require 'sidekiq/web'

Rails.application.routes.draw do

  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end

  # tell devise that users management are under accounts path
  devise_for :users, path: 'accounts', :controllers => {:registrations => "public/registrations", :sessions => "sessions"}


  scope :module => 'public' do

    if Rails.env.development?
      get 'welcome/index'
      get "/search" => "professionals#index"
      get "/professionals/search" => "professionals#index"
      get "/professionals/:id" => "doctors#profile", as: :doctor_public_profile
      ## TODO, except for new
      authenticate :user do
        resources :appointments
        get "/profile" => "profile#index"
        get "/settings" => "settings#index"
      end
    else
      get '*' => redirect('/professional/welcome')
    end

    namespace :company do
      # common pages
      get "/" => 'pages#index'
      # contact
      match "/contact" => "contact#index", :as => 'contact_us', via: [:get, :post]
      # features
      match "/features" => "features#index", :as => 'features', via: [:get, :post]
      # product
      get "/product/:page" => "product#pages", :as => 'product_pages'

      # default
      get "/:page" => 'pages#pages', :as => 'pages'
    end

    get "/guide/:page" => "guide#pages", :as => 'guide_pages'

  end


  draw :api

  # professional

  namespace :professional do
    get '/welcome', to: 'welcome#index'
    get '/', to: redirect('professional/dashboard')

    authenticate :user do
      namespace :dashboard do
        get '/', to: 'dashboard#index'
        resources :appointments
        resources :professional_holidays, :path => "/holidays"
        scope as: :scheduler, path: :scheduler do
          post '/update', to: 'professional_availability#update'
          get '/', to: 'professional_availability#index'
        end
        resources :professional_configs, only: [:index, :update]
        get '/pending', to: 'dashboard#pending'
        post '/request-confirmation', to: 'dashboard#request_confirmation'
      end

      namespace :vault do
        resources :documents
        get '/', to: 'vault#index'
      end
    end

    # devise handling for doctor
    devise_scope :user do
      get 'account/new', to: 'account#new', as: 'new_registration'
      post 'account/create', to: 'account#create', as: 'registration'
      match "account/register-as-professional", to: 'account#register_as_professional', as: :register_as_professional, via: [:get, :post]
      authenticate :user do
        get 'profile', to: 'profile#index', as: :profile

        patch 'profile/info', to: 'profile#info', as: :profile_info
        patch 'profile/address', as: :profile_address


        # specialities
        #
        patch 'profile/specialities', as: :profile_specialities

        post 'profile/documents', as: :profile_documents
        post 'profile/update'


        # specialities
        #
        patch 'profile/security', as: :profile_security

        get 'profile/confirm-request-required-documents', to: 'profile#confirm_profile'
        post 'profile/confirm-request-required-documents', to: 'profile#confirm_profile_request'
      end
    end
  end


  authenticate :user do
    namespace 'admin', module: :admin do
      root 'dashboard#index'
      resources :professionals do
        get 'requests'
        member do
          post 'confirm-request'
          post 'decline-request'
          match 'block-account', via: [:get, :post]
          post 'unblock-account'
        end

      end
    end
  end



  if not Rails.env.development?
    root 'professional/welcome#index'
  else
    root 'public/welcome#index'
  end

  # settings
  #
  get "/status/admin-not-found" => "app_status#admin_not_found"


  constraints Constraints::MonitoringWhiteListConstraint.new do
    mount Sidekiq::Web => '/sidekiq'
  end

end
