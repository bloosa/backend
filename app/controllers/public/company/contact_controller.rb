class Public::Company::ContactController < ApplicationController
  def index
    if request.post?
      handle_contact
    end
  end


  private

  def handle_contact
    contact = Contact.new(contact_us_form)
    if contact.save
      flash[:success] = I18n.t("company.contact_us.flash_message_contact_us")
      UserMailer.new_contact(contact.name, contact.email, contact.body).deliver_now
      AdminMailer.new_contact(contact.name, contact.email, contact.body).deliver_now
      redirect_to root_path
    else
      @errors = contact.errors
    end
  end

  def contact_us_form
    params.fetch(:contact, {}).permit([:email, :body, :name, :organisation])
  end


end
