class Public::Company::FeaturesController < ApplicationController
  def index
    if request.post?
      handle_form
    end
  end

  private

  def handle_form
    feature_request = FeatureRequest.new(request_feature_form)
    if feature_request.save
      flash[:success] = I18n.t("feature.flash_message_feature_request")
      name = feature_request.sender_name
      email = feature_request.sender_email
      feature = feature_request.feature
      details = feature_request.feature_details
      UserMailer.new_feature_request(name, email, feature, details).deliver_now
      AdminMailer.new_feature_request(name, email, feature, details).deliver_now
      redirect_to root_path
    else
      @errors = feature_request.errors
    end

  end


  def request_feature_form
    params.fetch(:feature, {}).permit([:sender_email, :sender_name, :feature, :feature_details])
  end


end
