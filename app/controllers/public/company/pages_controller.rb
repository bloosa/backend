class Public::Company::PagesController < ApplicationController
  def index
    redirect_to company_pages_path('about')
  end

  def pages
    render_static("public/company/pages")
  end

end
