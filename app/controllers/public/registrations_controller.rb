class Public::RegistrationsController < Devise::RegistrationsController
  before_action :configure_permitted_parameters, if: :devise_controller?

  def new
    super do
      resource.build_user_profile
    end
  end


  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password, :password_confirmation,
                                                       {user_profile_attributes: [:name, :last_name, :gender, :birthday, :address, :zip_code]}])
  end

end