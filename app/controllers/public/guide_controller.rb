class Public::GuideController < ApplicationController

  def pages
    render_static("public/guide")
  end
end
