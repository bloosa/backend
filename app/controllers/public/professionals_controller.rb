class Public::ProfessionalsController < ApplicationController

  def search

  end

  def profile
    @doctor = DoctorProfile.find_by_user_id(params[:id])
    unless @doctor && @doctor.is_confirmed?
      render :file => 'public/500.html', :status => :not_found, :layout => false
      return
    end
    @specialites = @doctor.specialities
  end

end
