class Admin::ProfessionalsController < Admin::AdminController

  before_action :set_doctor_profile, :only => [:show, :confirm_request, :decline_request, :block_account], raise: false

  def index
    query = request.query_parameters
    @query = query[:q]
    specialities_ids = []
    if @query.present?
      specialities_ids = Speciality.find_by_field_query(@query).map(&:id)
    end

    @doctors = DoctorProfile
                   .eager_load(:doctor_profile_status, :specialities, :user => [:user_profile])


    ###
    # .joins(:doctor_profile_status)
    #                    .where(doctor_profile_statuses: {status: 'REQUEST'})
    #
    #
    if @query.present?
      @doctors = DoctorProfile
                     .joins(:user => :user_profile) #TODO, fix sanitzation
                     .where("user_profiles.name like '%#{@query}%'")
                     .joins(:specialities)
                     .uniq
      "" ".where(:doctors_specialities => {:speciality_id => specialities_ids})
                     " ""
    end
  end

  def show

  end

  # get all requests for doctor profile
  def requests
    ids = DoctorProfileStatus.where(status: PROFILE_STATUES.request).map(&:id)
    @profiles = DoctorProfile.where(confirmation_status_id: ids).eager_load(:user, :doctor_profile_status, :specialities)
  end


  def block_account
    @professional_block_account_history = ProfessionalBlockAccountHistory.new
    professional_id = params[:id]
    user = User.find(professional_id)
    unless user.nil?
      if user.is_professional?
        @user = user
        # TO, check for existing
        @professional_block_account_history.professional_id = professional_id
        if request.post?
          professional_block = ProfessionalBlockAccountHistory.new(block_account_params)
          professional_block.blocked_by_id = current_user.id
          professional_block.professional_id = professional_id
          if professional_block.save
            user.block_professional_account()
            AdminMailer.block_professional_account(user.id, professional_block.blocking_reason).deliver_now!
            DoctorMailer.account_blocked_notification(user.id, professional_block.blocking_reason).deliver_now!
            flash[:notice] = I18n.t('admin.flash.account_blocked_success')
            redirect_to admin_professionals_path
          else
            @errors = professional_block.errors
          end
        end
      else
        flash[:notice] = I18n.t('This use is not a professional')
        redirect_to admin_professionals_path
      end
    end

  end

  def unblock_account
    redirect_to admin_professionals_path
  end

  # get all requests for doctor profile
  def confirm_request
    message = params[:message]
    @doctor_profile.doctor_profile_status =
        DoctorMailer.send_confirmation_request(@doctor_profile.user_id, message).deliver_later
  end

  def decline_request
    message = params[:message]
    #TODO
    # DoctorMailer.send_decline_request(@doctor_profile.user_id, message).deliver_later
  end

  protected

  def set_doctor_profile
    @doctor_profile = DoctorProfile.find_by_user_id(params[:id])
    # TODO, rais if not exists
  end


  def block_account_params
    params.fetch(:professional_block_account_history, {}).permit([:user_id, :blocking_reason])
  end
end
