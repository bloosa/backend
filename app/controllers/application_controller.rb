class ApplicationController < ActionController::Base
  before_action :check_admin
  # include Sidekiq
  include ApplicationHelper
  protect_from_forgery

  def index

  end

  def after_sign_in_path_for(resource_or_scope)
    if current_user.is_doctor?
      professional_dashboard_path
    elsif current_user.is_doctor?
      admin_dashboard_path
    else
      root_path
    end
  end

  def check_admin
    return #TODO
    unless User.exists?(:role => Role.find_by_name('admin'))
      redirect_to '/status/admin-not-found'
    end
  end

end
