module Api
  module V1
    class ProfessionalsController < BaseController
      include SearchHelper

      def search
        unless params[:location].nil?
          city = City.find_by_id(params[:location])
        end
        @doctors = User
                       .select("users.id, users.email")
                       .eager_load(:user_profile, doctor_profile: {specialities: :speciality})
                       .joins(:professional_holidays)
                       .where("DATE '#{Date.new(2019, 8, 20)}' NOT BETWEEN professional_holidays.start_at AND professional_holidays.end_at")
                       .joins(:user_profile)
                       .joins(:doctor_profile)
                       .where({doctor_profiles: {is_confirmed: true}})
                       .where(doctor_profiles_close_to_query(35.796003, 10.635681, 4000))


        if params[:specialities]
          @doctors = @doctors
                         .joins(:doctors_specialities)
                         .where(:doctors_specialities => {:speciality_id => params[:specialities]})

        end

        @doctors = @doctors.uniq

        "" "
        .select(:id)
                       .eager_load(:doctor_profile)
                       .joins(:doctor_profile)
                       .where({doctor_profiles: {is_confirmed: true}})

                       .joins(:specialities)
                       .where(:doctors_specialities => {:speciality_id => specialities_ids})
                       .uniq
" ""
        render :json => {data: @doctors.as_json(only: [:id],
                                                include: {
                                                    doctor_profile: {
                                                        only: [:full_address, :latitude, :longitude, :address],
                                                        include: {
                                                            specialities: {
                                                                only: [],
                                                                include: {
                                                                    speciality: {
                                                                        only: [:id, :en_fr, :en_ar]
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    },
                                                    user_profile: {only: [:name, :last_name, :title]},
                                                })}
      end


      def recommended
        @doctors = User.all
        render :json => {:data => @doctors}
      end


      def profile
        @doctors = DoctorProfile.all
        render :json => {:data => @doctors}
      end

      def vote

      end

    end
  end
end
