module Api
  module V1
    class MessagesController < BaseController

      def create
        ActiveRecord::Base.transaction do
          conversation = Conversation.new
          conversation.add_new_message(current_user, message_params[:to], message_params[:body])
        end
        flash[:notice] = "message sent "
        respond_to do |format|
          puts format
          puts format
          format.html do
            redirect_to public_path
          end
          format.json :json => {:ok => true}
        end
      rescue ActiveRecord::RecordInvalid
        puts "Oops. We tried to do an invalid operation!"

      end

      private

      def message_params
        params.fetch(:message, {}).permit([:to, :body])
      end

    end
  end
end
