module Api
  module V1
    class GeoController < BaseController
      #caches_action :index, :layout => false
      def cities
        @cities = City.all
        if stale?(etag: @cities, :last_modified => @cities.maximum(:created_at), public: true)
          results = ::Rails.cache.fetch('cities', :expires_in => 10.days) do
            @cities.to_json
          end
          render :json => {:list => JSON.load(results)}
        end
      end

      #caches_action :index, :layout => false
      def index
        cities
      end
    end
  end
end
