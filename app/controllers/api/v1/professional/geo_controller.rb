module Api
  module V1
    module Professional
      class GeoController < BaseController
        #caches_action :index, :layout => false
        def reverse
          results = Geocoder.search("#{params[:lat]}, #{params[:lng]}")
          render :json => {:list => results}
        end

        #caches_action :index, :layout => false
        def index
          cities
        end
      end
    end
  end
end
