module Api
  module V1
    class SpecialitiesController < BaseController
      def index
        @specialities = Speciality.all
        render :json => {:list => @specialities}
      end
    end
  end
end
