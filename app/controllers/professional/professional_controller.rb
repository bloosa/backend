class Professional::ProfessionalController < ApplicationController
  include ProfessionalHelper
  before_action :is_doctor?
  before_action :idle_profile_status?
  before_action :waiting_confirmation_status?
  layout 'doctor'
end