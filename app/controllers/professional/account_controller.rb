class Professional::AccountController < Devise::RegistrationsController
  include ProfessionalHelper

  #https://stackoverflow.com/questions/41266207/before-process-action-callback-authenticate-user-has-not-been-defined#41266746
  skip_before_action :is_doctor?, :only => [:create, :register_as_professional], raise: false
  skip_before_action :idle_profile_status?, :only => [:create, :register_as_professional], raise: false

  before_action :configure_permitted_parameters, if: :devise_controller?

  def new
    super do
      resource.build_user_profile
      resource.build_doctor_profile
    end
  end

  def create
    super do
      if resource.persisted?
        send_request_doctor_mail(resource)
      end
    end
  end

  # A user already create an account but
  def register_as_professional
    if current_user
      if request.method === "POST"
        @profile = current_user.create_doctor_profile!()
        puts (@profile)
        ## @profile TODO add
        if @profile.save!
          flash[:success] = t("Request for becoming doctor is confirmed")
          # Tell the UserMailer to send a welcome email after save
          send_request_doctor_mail(current_user)
          redirect_to :doctor_profile
        else
          flash[:error] = t('Something went wrong !')
        end
      else
        # if get, build profile
        @profile = current_user.build_doctor_profile
      end
    else
      redirect_to :doctor_new_registration
    end

  end

  protected

  def after_sign_up_path_for(resource)
    :doctor_profile_update
  end

  def send_request_doctor_mail (user)
    AdminMailer.request_for_doctor_account(user.id).deliver_later
    UserMailer.request_doctor_profile(user.id).deliver_later
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password, :password_confirmation,
                                                       {user_profile_attributes: [:name, :last_name, :birthday, :address, :zip_code]},
                                                       {doctor_profile_attributes: [:street, :latitude, :longitude, :full_address, :zip_code]}])
  end

end
