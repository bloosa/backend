class Professional::WelcomeController < Professional::ProfessionalController
  layout 'application'

  # TODO : VERIV_01, remove comment from here
  skip_before_action :is_doctor?, :idle_profile_status?, :waiting_confirmation_status?
  before_action :is_already_business_account?

  def index

  end

  protected

  def is_already_business_account?

  end

end
