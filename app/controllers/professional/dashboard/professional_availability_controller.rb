class Professional::Dashboard::ProfessionalAvailabilityController < Professional::ProfessionalController
  before_action :set_professional_config, only: [:index, :update]
  # GET /doctor_availability
  def index
    @doctor_availability = DoctorAvailability.all
    @specialites = current_user.doctor_profile.specialities.eager_load(:speciality)

    puts @specialites
    ###
    @times_slots_by_speciality_id = current_user.doctor_profile.doctor_speciality_estimation_times.index_by(&:speciality_id)
    puts "@times_slots_by_speciality_id"
    puts "@times_slots_by_speciality_id"
    puts "@times_slots_by_speciality_id"
    puts "@times_slots_by_speciality_id"
    puts @times_slots_by_speciality_id

    @ref_time_morning_start = Time.new(2020, 01, 31, 8, 0, 0, "+02:00")
    @ref_time_morning_end = @ref_time_morning_start + 5.hours


    @ref_time_evening_start = @ref_time_morning_end
    @ref_time_evening_end = @ref_time_evening_start + 5.hours
  end

  def update
    # update slots and disponibliyt
    #
    professional_config_attr =  professional_config_params[:professional_config]

    @professional_config.update!(professional_config_attr)


    ## {"1"=>"10", "2"=>"10", "3"=>"10"}
    speciality_by_time = consultation_duration_by_slot_params

    puts '##############'
    puts '##############'
    puts '##############'
    puts '##############'
    puts speciality_by_time
    @specialites_and_times = current_user.doctor_profile.doctor_speciality_estimation_times
    speciality_by_time.each do |speciality_id, time|
      DoctorSpecialityEstimationTime.
          find_or_initialize_by(:speciality_id => speciality_id, :professional_id => current_user.id).
          update_attributes!(:time => time)
    end
    redirect_to professional_dashboard_scheduler_path
  end

  private


  # Use callbacks to share common setup or constraints between actions.
  def set_professional_config
    @professional_config = ProfessionalConfig.find_or_create_by({:professional_id => current_user.id})

  end


  def professional_config_params
    params.fetch(:config, {}).permit([
                                           professional_config: [:max_days_from_today_to_be_booked, :slot_consultation_duration_min]])
  end


  def consultation_duration_by_slot_params
    params.fetch(:consultation_duration, {}).permit!

  end
end
