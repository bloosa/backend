class Professional::Dashboard::DashboardController < Professional::ProfessionalController
  skip_before_action :waiting_confirmation_status?, :only => [:pending, :request_confirmation]

  def index
    render 'professional/dashboard/index'
  end

  def pending
    render 'professional/dashboard/pending', :layout => false
  end

  def request_confirmation
    user_id = current_user.id
    AdminMailer.request_for_doctor_account_confirmation(user_id).deliver_now
    UserMailer.request_doctor_profile_confirmation(user_id).deliver_later
  end
end
