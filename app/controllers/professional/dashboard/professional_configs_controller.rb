class Professional::Dashboard::ProfessionalConfigsController < Professional::ProfessionalController

  before_action :set_professional_config, only: [:index, :update]


  def index
    render 'update'
  end

  # POST /professional_configs
  def update

    @professional_config.update(update_params.except(:user))

    user_params = update_params[:user]

    current_user.update!(user_params)

    if current_user.phone_number_changed?
      current_user.two_factor_via_confirmed = false
    end

    redirect_to professional_dashboard_professional_configs_path
  end


  private



  # Never trust parameters from the scary internet, only allow the white list through.
  def update_params

  end

end
