class Professional::Dashboard::ProfessionalHolidaysController < Professional::ProfessionalController

  before_action :set_doctor_holiday, only: [:show, :edit, :update, :destroy]


  def index
    holidays = current_user.professional_holidays.where.not(start_at: [nil]).all
    @holidays = holidays.sort_by {|obj| obj.start_at}
    start = Date.current.beginning_of_month
    future_date = (Date.current + 8.months).end_of_month
    @range = (start..future_date).select {|d| d.day == 1}

  end

  # GET /doctor_availability/new
  def new
    @professional_holiday = ProfessionalHoliday.new

  end

  # POST /doctor_availability/create
  def create
    @professional_holiday = ProfessionalHoliday.new(holidays_create_params)
    current_start_at = @professional_holiday.start_at
    current_end_at = @professional_holiday.end_at
    @professional_holiday.professional_id = current_user.id

    if current_start_at.nil? && !current_end_at.nil?
      @professional_holiday.errors[:base] << t("doctor.dotor_holidays.errors.null_values")
    elsif current_start_at >= current_end_at
      @professional_holiday.errors[:base] << t("doctor.dotor_holidays.errors.end_at_should_be_greater_than_start_at")
    end
    # Check for appointments
    if !@professional_holiday.errors.any? && @professional_holiday.valid?
      @professional_holiday.save!
      redirect_to professional_dashboard_professional_holidays_path, notice: 'Your holiday was successfully created.'
    else

      render :new
    end

  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_doctor_holiday
    @professional_holiday = DoctorAvailability.find(params[:id])
  end


  # Never trust parameters from the scary internet, only allow the white list through.
  def holidays_create_params
    params.fetch(:professional_holiday, {}).permit([:start_at, :end_at])
  end

end
