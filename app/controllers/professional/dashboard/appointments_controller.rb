class Professional::Dashboard::AppointmentsController < Professional::ProfessionalController
  def index

    @consultations = current_user.consultations
    @month = Date.current.beginning_of_month
    unless params[:month].nil?
      begin
        @month = Date.strptime(params[:month], "%Y-%m-%d")
      rescue
        nil
      end
    end

  end

  # confirm appointment
  # Check for disponiblity
  # disable disponibilites
  def confirm

  end
end
