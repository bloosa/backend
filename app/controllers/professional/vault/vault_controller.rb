class Professional::Vault::VaultController < Professional::ProfessionalController
  def index
    render 'professional/vault/index'
  end

  private

  def profile_request_params
    params.require(:doctor_profile).permit(files: [], speciality_ids: [])
  end


  def update_params
    params.require(:doctor_profile).permit(:city, :avatar, speciality_ids: [])
  end
end
