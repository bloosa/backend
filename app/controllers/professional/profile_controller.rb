class Professional::ProfileController < Professional::ProfessionalController
  skip_before_action :idle_profile_status?
  before_action :set_profile
  before_action :set_tab, except: [:confirm_profile, :confirm_profile_request]
  before_action :check_tab_exist, only: :index

  def index

  end


  def info
    if request.patch?
      # we update the user_profile address_params not the doctor profile
      current_user.profile.update(user_info_params)
      redirect_to action: :index
    end
  end

  def security
    if request.patch?
      # we update the user_profile address_params not the doctor profile
      @doctor_profile.update(security_params.except(:user))
      user_params = {otp_required_for_login: false}.merge(security_params[:user])
      @doctor_profile.user.update!(user_params)
      redirect_to action: :index
    end
  end

  def address
    if request.patch?
      ## Message should sent here
      @doctor_profile.update(address_params)
      redirect_to action: "index"
    end
  end

  def specialities
    if request.post?
      ## Message should sent here
      @doctor_profile.update(update_params)
      redirect_to action: "index"
    end
  end


  def confirm_profile

  end

  def confirm_profile_request
    files = profile_request_params[:files]
    specialities = profile_request_params[:speciality_ids]
    if files&.length and specialities&.length
      @doctor_profile.files.attach(files)
      @doctor_profile.update_to_request_status
      update_profile_specialities(specialities)
      @doctor_profile.save!
      AdminMailer.confirmation_docs_sent(current_user.id).deliver_now
      redirect_to professional_dashboard_path
    else
      flash[':error'] = t('Empty or speciality  files')
    end
  end


  protected

  def set_profile
    @doctor_profile = DoctorProfile.where(user: current_user).eager_load(:user => [:profile]).with_attached_avatar.first

  end

  def update_profile_specialities(specialities)
    specialities.each do |speciality|
      DoctorsSpeciality.find_or_create_by({:professional_id => @doctor_profile.user.id, :speciality_id => speciality})
    end
  end


  private

  def profile_request_params
    params.require(:doctor_profile).permit(files: [], speciality_ids: [])
  end


  def user_info_params
    params.fetch(:user_profile, {}).permit(:name, :last_name, :gender, :title)
  end


  def address_params
    params.require(:doctor_profile).permit(:city, :zip_code, :full_address, :city_id)
  end


  def specialities_params
    params.require(:doctor_profile).permit(speciality_ids: [])
  end

  def security_params
    params.require(:doctor_profile).permit(user: [:otp_required_for_login, :two_factor_via, :phone_number])
  end


  def set_tab
    @tab = params[:tab] || 'info'
  end

  def check_tab_exist
    unless file_exists_in_app?("app/views/professional/profile/_#{@tab}.html.erb")
      redirect_to '404'
    end
  end

end
