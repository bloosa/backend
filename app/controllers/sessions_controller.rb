class SessionsController < Devise::SessionsController
  #include AuthenticateWithTwoFactor
  #prepend_before_action :authenticate_with_two_factor, if: -> {action_name == 'create' && two_factor_enabled?}

  def new
    super
  end

  protected

  def two_factor_enabled?
    find_user&.otp_required_for_login?
  end


  def find_user
    if session[:otp_user_id]
      User.find(session[:otp_user_id])
    elsif user_params[:email]
      User.find_by_email(user_params[:email])
    end
  end

  def valid_otp_attempt?(user)
    user.validate_and_consume_otp!(user_params[:otp_attempt]) ||
        user.invalidate_otp_backup_code!(user_params[:otp_attempt])
  end

  private

  def user_params
    params.fetch(:user, {}).permit([:email, :password, :remember_me, :otp_user_id, :otp_attempt])
  end
end