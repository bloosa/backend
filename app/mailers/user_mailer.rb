class UserMailer < ApplicationMailer
  default from: 'hello@bloosa.tn'

  def welcome_email(user_id)
    @user = User.find_by_id(user_id)
    if @user
      @url = 'http://wb.tn/accounts/login'
      mail(to: @user.email, subject: 'Welcome to Bloosa')
    end
  end

  def request_doctor_profile(user_id)
    @user = User.find_by_id(user_id)
    if @user
      @url = 'http://wb.tn/doctor/login'
      mail(to: @user.email, subject: 'Request sent to us : profile doctor  ')

    end
  end

  def new_contact(name, email, body)
    @name = name
    @body = body
    mail(to: email, subject: I18n.t('email.contact.subject'))
  end


  def new_feature_request(name, email, feature, details)
    @name = name
    @feature = feature
    @feature_details = details
    mail(to: email, subject: I18n.t('email.feature.email_subject'))
  end

  def request_doctor_profile_confirmation(user_id)
    @user = User.find_by_id(user_id)
    if @user
      @url = 'http://wb.tn/doctor/login'
      mail(to: @user.email, subject: 'Request sent to us : pending confirmation ')
    end
  end

end
