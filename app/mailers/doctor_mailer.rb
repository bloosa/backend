class DoctorMailer < ApplicationMailer
  # A user request appointement
  def request_appointment

  end

  # an appointment notfication
  def notify_appointment

  end

  # an appointment notfication
  def send_confirmation_request(doctor_id, email_message)
    @user = User.find_by_id(doctor_id)
    if @user
      mail(to: @user.email, subject: t('Profile confirmed !'))
    end
  end


  # send_block_account
  def account_blocked_notification(professional_id, blocking_reason)
    @user = User.find_by_id(professional_id)
    if @user
      @blocking_reason = blocking_reason
      mail(to: @user.email, subject: t('Account blocked !'))
    end
  end


end
