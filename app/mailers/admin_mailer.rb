class AdminMailer < ApplicationMailer
  default from: Rails.application.config.x.app_mail_config[:contact_mail]
  @@admin_mail = Rails.application.config.x.app_mail_config[:admin_mail]


  # A request for feature
  def new_feature_request(name, email, feature, details)
    @name = name
    @email = email
    @feature = feature
    @feature_details = details
    mail(to: @@admin_mail, subject: I18n.t('email.feature.admin_email_subject'))
  end


  # A new contact message
  def new_contact(name, email, body)
    @email = email
    @name = name
    @body = body
    mail(to: @@admin_mail, subject: 'New contact !!')
  end


  # A doctor request to confirm it's account
  def block_professional_account(user_id, blocking_reason)
    @user = User.find_by(id: user_id)
    if @user
      @blocking_reason = blocking_reason
      mail(to: @@admin_mail, subject: 'Professional profile suspension')
    end
  end


  # A doctor request to confirm it's account
  def request_for_doctor_account(user_id)
    @user = User.find_by(id: user_id)
    if @user
      mail(to: @@admin_mail, subject: 'Doctor profile activation request ')
    end
  end

  # A doctor request pending confirmation
  def request_for_doctor_account_confirmation(user_id)
    @user = User.find_by(id: user_id)
    if @user
      mail(to: @@admin_mail, subject: 'Doctor profile request pending confirmation ')
      puts ("emails send to #{@@admin_mail}")
    end
  end


  # A doctor called to confirm it's account
  def confirmation_docs_sent(user_id)
    @user = User.find_by(id: user_id)
    if @user
      mail(to: @@admin_mail, subject: 'Confirmation docs sent from doctor')
      puts ("emails send to #{@@admin_mail}")
    end
  end

end
