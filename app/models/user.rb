class User < ActiveRecord::Base

  before_create :set_default_role
  after_create :init_profile
  after_initialize :set_default_role, :if => :new_record?

  #devise :two_factor_authenticatable,
  #      :otp_secret_encryption_key => ENV['BLOOSA_OTP_ENCRYPTION_KEY']
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  # note, we removed the :database_authenticatable to activate encryption_key_env
  devise :database_authenticatable, :registerable, #, :database_authenticatable,
         :confirmable, :recoverable, :rememberable, :trackable, :validatable


  belongs_to :role

  has_one :profile, class_name: 'UserProfile', dependent: :destroy
  has_one :user_profile, dependent: :destroy
  accepts_nested_attributes_for :user_profile


  ### appointments
  has_many :appointments, dependent: :destroy, foreign_key: :patient_id


  ################# Doctor configuration
  has_one :doctor_profile, dependent: :destroy
  accepts_nested_attributes_for :doctor_profile
  ### specialitites
  has_many :doctors_specialities, foreign_key: :doctor_id, dependent: :destroy
  ### specialitites
  has_many :specialities, through: :doctors_specialities
  ### specialitites
  has_many :professional_holidays, foreign_key: :professional_id, dependent: :destroy
  ### avalabilities
  has_many :doctor_availabilities, foreign_key: :doctor_id, dependent: :destroy
  ###
  has_many :doctor_speciality_estimation_times, foreign_key: :professional_id, dependent: :destroy

  ### doctor
  has_many :consultations, foreign_key: :doctor_id, class_name: 'Appointment'


  ## Conversation
  has_many :authored_conversations, class_name: 'Conversation', foreign_key: 'author_id'
  has_many :received_conversations, class_name: 'Conversation', foreign_key: 'received_id'

  ## Messages
  has_many :conversation_messages, dependent: :destroy

  ## Doctor votes

  ## Messages
  has_many :doctor_votes, dependent: :destroy


  ## professional_config
  has_one :professional_config, foreign_key: :professional_id, dependent: :destroy


  attr_encrypted :phone_number, key: ENV['BLOOSA_PHONE_NUMBER_ENCRYPTION_KEY']
  validates_presence_of :phone_number, :if => :otp_required_for_login?

  def is_doctor?
    DoctorProfile.where(user: self).any?
  end

  def is_professional?
    DoctorProfile.where(user: self).any?
  end

  def is_professional_blocked?
    doctor_profile = DoctorProfile.where(user: self).first
    !doctor_profile.is_confirmed && doctor_profile.doctor_profile_status === DoctorProfileStatus.find_by_status('BLOCKED')
  end

  def block_professional_account
    doctor_profile = DoctorProfile.where(user: self).first
    doctor_profile.is_confirmed = false
    doctor_profile.doctor_profile_status = DoctorProfileStatus.find_by_status('BLOCKED')
    doctor_profile.save!
  end

  def is_admin?
    self.role&.id == Role.find_by_name('admin').id
  end

  def role?(role_name)
    role == role_name
  end

  #TODO , look for banned or something similar
  def can_login?(ability = '')
    true
  end

  # TODO
  def active_for_authentication?
    true
  end

  protected


  private


  def set_default_role
    self.role ||= Role.find_by_name('patient')
  end

  def init_profile
    # Tell the UserMailer to send a welcome email after save
    UserMailer.welcome_email(self.id).deliver_later!
  end

  def valid_two_factor_confirmation
    return true unless two_factor_just_set || phone_changed_with_two_factor
    self.unconfirmed_two_factor = true
  end

  def send_two_factor_authentication_code(code)
    # Send code via SMS, etc.
    puts code
  end

  def need_two_factor_authentication?(request)
    request.ip != '127.0.0.1'
  end


end
