class ProfessionalConfig < ApplicationRecord
  belongs_to :professional, class_name: "User"
  validates_uniqueness_of :professional_id
end
