class Conversation < ApplicationRecord
  belongs_to :initializer, class_name: 'User'
  belongs_to :participant, class_name: 'User'
  validates :initializer, uniqueness: {scope: :participant}
  validates :participant, uniqueness: {scope: :initializer}
  has_many :conversation_messages, -> {order(created_at: :asc)}, dependent: :destroy
  validates_presence_of :initializer, :participant

  def initialize_or_create(me, to_user_id)
    # code here
    initializer = me
    participant = User.find(to_user_id)


    if to_user_id === initializer.id
      errors.add(:base, 'same user')
      return
    end

    # check for user
    if participant.nil?
      errors.add(:base, 'user does not exists')
      return
    end


    exists = Conversation.exists?(['(initializer_id = :from_id AND participant_id = :to_id) OR
                          (initializer_id = :to_id AND participant_id = :from_id)',
                                   {from_id: initializer.id, to_id: participant.id}])


    unless exists
      self.participant = participant
      self.initializer = initializer
      self.save!
    end


  end

  def add_new_message(sender, to_user_id, message)
    self.initialize_or_create(sender, to_user_id)
    unless errors.any?
      new_message = ConversationMessage.new
      new_message.conversation = self
      new_message.body = message
      new_message.sender_id = sender.id
      new_message.save!
    end
  end

end
