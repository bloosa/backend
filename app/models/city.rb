class City < ApplicationRecord
  validates :name, uniqueness: {scope: [:latitude, :longitude]}
  validates :country_id, uniqueness: {scope: [:latitude, :longitude]}

  has_many :doctors, through: :doctor_profiles, :class_name => "User"

  belongs_to :country

  scope :close_to_query, -> (latitude, longitude, distance_in_meters = 2000) {
    where(%{
    ST_DWithin(
      ST_GeographyFromText(
        'SRID=4326;POINT(' || cities.longitude || ' ' || cities.latitude || ')'
      ),
      ST_GeographyFromText('SRID=4326;POINT(%f %f)'),
      %d
    )
  } % [longitude, latitude, distance_in_meters])
  }
end
