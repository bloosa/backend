class DoctorVote < ApplicationRecord
  ## votes to me as doctor
  belongs_to :user, foreign_key: :doctor_id, :counter_cache => :votes_to_me_count
  ## my votes
  belongs_to :user, :counter_cache => :total_votes_given_by_me

  validates_uniqueness_of :doctor_id, :scope => :user_id

end
