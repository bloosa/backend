class DoctorProfileStatus < ActiveRecord::Base
  validates :status, uniqueness: true
end
