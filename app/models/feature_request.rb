class FeatureRequest < ApplicationRecord
  validates_presence_of :feature
  validates_presence_of :feature_details
  validates_presence_of :sender_email
  validates_presence_of :sender_name


  validates :sender_email, format: {with: URI::MailTo::EMAIL_REGEXP}
end
