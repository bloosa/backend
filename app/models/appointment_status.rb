class AppointmentStatus < ActiveRecord::Base
  validates :status, uniqueness: true
end
