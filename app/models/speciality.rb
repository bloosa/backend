class Speciality < ActiveRecord::Base
  validates :en_fr, uniqueness: true


  scope :find_by_field_query, ->(query) do
    where(arel_table[:en_fr].matches("%#{sanitize_sql_like(query)}%"))
  end
end
