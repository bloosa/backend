class DoctorAvailability < ActiveRecord::Base
  belongs_to :users, foreign_key: "doctor_id"
  validates_presence_of :day
  validates_uniqueness_of :period, scope: [:doctor_id, :day]

  after_validation :geocode, if: ->(obj) {obj.address.present? and obj.address_changed?}

  after_validation :reverse_geocode, unless: ->(obj) {obj.address.present?},
                   if: ->(obj) {obj.latitude.present? and obj.latitude_changed? and obj.longitude.present? and obj.longitude_changed?}


  geocoded_by :address

  reverse_geocoded_by :latitude, :longitude, :address => :fill_address do |obj, results|
    if geo = results.first
      obj.city = geo.city
      obj.zip_code = geo.postal_code
      obj.country = geo.country_code
    end
  end

  def address
    [street, zip_code, city, country].compact.join(', ')
  end

end
