class DoctorProfile < ActiveRecord::Base
  before_save :default_values
  has_one_attached :avatar
  has_many_attached :files
  after_validation :geocode


  # User
  belongs_to :user

  # Specialities
  has_many :specialities, :through => :user, source: :doctors_specialities, dependent: :destroy
  accepts_nested_attributes_for :specialities

  # Time for Specialities
  has_many :doctor_speciality_estimation_times,
           :through => :user,
           source: :doctor_speciality_estimation_times,
           dependent: :destroy,
           :foreign_key => :professional_id


  belongs_to :doctor_profile_status, foreign_key: :confirmation_status_id
  belongs_to :city
  belongs_to :country

  validates_uniqueness_of :user_id

  geocoded_by :address

  scope :close_to_query, -> (latitude, longitude, distance_in_meters = 2000) {
    where(SearchHelper.close_to(latitude, longitude, distance_in_meters))
  }

  # Update to request
  def update_to_request_status
    self.confirmation_status_id = DoctorProfileStatus.find_by_status('REQUEST').id
  end

  def idle_profile_status?
    self.confirmation_status_id === DoctorProfileStatus.find_by_status('IDLE').id
  end

  # Waiting confirmation request status check
  def waiting_confirmation_status?
    self.confirmation_status_id == DoctorProfileStatus.find_by_status('REQUEST').id
  end


  def default_values
    self.confirmation_status_id = DoctorProfileStatus.find_by_status('IDLE').id if self.confirmation_status_id.nil? # note self.status = 'P'  might be safer (per @frontendbeauty)
  end


  def address
    if city.nil? && (full_address.nil? || full_address.empty?)
      return nil
    end
    _address = [full_address, street, zip_code].compact.join(', ')
    unless city.nil?
      _address = [_address, city.name, city.country.name].compact.join(', ')
    end
    _address
  end

  def is_confirmed?
    self.is_confirmed && self.confirmation_status_id == DoctorProfileStatus.find_by_status('APPROVED').id
  end


end
