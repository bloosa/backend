class ProfessionalHoliday < ApplicationRecord
  validates_presence_of :end_at, :start_at
  validate :active_period, on: :create

  # add an error if there's a period already exists in same
  "" "
      [C,D] and [A,B] will NOT overlap if the D < A OR C > B
              A------B
      C---D             C---D
       ===>  !(end_at < start_at([A,B]) || start_at >  end_at([A,B]))
       ===>  end_at > start_at([A,B]) &&  start_at <  end_at([A,B])

      Look for possible intersections

" ""

  def active_period
    exists = ProfessionalHoliday.where('start_at < ? AND end_at > ? AND professional_id = ?', self.end_at, self.start_at, self.professional_id).exists?
    errors.add(:base, I18n.t("doctor.dotor_holidays.errors.holiday_already_exists_in_same_period")) if exists
  end
end
