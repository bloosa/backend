class ProfessionalBlockAccountHistory < ApplicationRecord
  belongs_to :user, foreign_key: :professional_id
  belongs_to :user, foreign_key: :blocked_by_id

  validates_presence_of :blocking_reason
  validates_presence_of :blocked_by_id
  validates_presence_of :professional_id
end
