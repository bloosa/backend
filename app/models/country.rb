class Country < ApplicationRecord
  validates :code, uniqueness: true
  validates :name, uniqueness: true
  validates :iso_code, uniqueness: true
end
