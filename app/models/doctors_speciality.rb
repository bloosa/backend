class DoctorsSpeciality < ActiveRecord::Base
  belongs_to :user, :foreign_key => :doctor_id
  belongs_to :speciality
  # TODO, add price and time avera

  validates_uniqueness_of :doctor_id, :scope => :speciality_id

  validates :speciality_id, presence: true, allow_blank: false
  validates :speciality_id, presence: true, allow_nil: false


  validates :doctor_id, presence: true, allow_blank: false
  validates :doctor_id, presence: true, allow_nil: false

end
