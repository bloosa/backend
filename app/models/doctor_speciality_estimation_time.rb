class DoctorSpecialityEstimationTime < ApplicationRecord
  validates_presence_of :professional_id, :speciality_id
  validates_uniqueness_of :professional_id, scope: :speciality_id
  validates_presence_of :time


  belongs_to :user, foreign_key: :professional_id
  belongs_to :speciality
end
