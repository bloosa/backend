module CalendarHelper
  def in_holiday?(day, events)
    events.detect {|entry| entry[:range].include?(day)}
  end

  def begin_of_holiday(day, holidays)
    holidays.detect {|entry| entry[:range].include?(day)}
  end

  # Find max interesction count
  # Find the day where the maximum of events
  def max_day_interersetion(events)
    # let say [1,3], [3,4], [0,5], here 3 is the maximum intersction
    # between the
    [(1..3), (3..4), (0..5)].each do |d|
      puts d
    end
  end

  def get_classes(day, events)
    puts events.select {|entry| entry[:range].include?(day)}
    puts "#################"
    events.select {|entry| entry[:range].include?(day)}
  end

  def gen_event_color(event_range)
    if event_range[:range].min
      gen_color_from_int(event_range[:range].min.yday)
    end
  end

  def gen_color_from_int(int)
    puts '#################'
    puts '#################'
    puts '#################'
    puts '#################'
    puts '#################'
    puts int
    int_rgb(hash_code(int.to_s))
  end

  def hash_code(str)
    hash = 0
    str.each_char do |entry|
      hash = entry.ord + ((hash << 5) - hash)
    end
    hash
  end

  def int_rgb(i)
    color = (i & 0x00FFFFFF).to_s(16).upcase
    "00000"[0, 6 - color.length] + color
  end
end