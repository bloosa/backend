module SearchHelper
  def doctor_profiles_close_to_query(longitude, latitude, distance_in_meters)
    %{
    ST_DWithin(
      ST_GeographyFromText(
        'SRID=4326;POINT(' || doctor_profiles.latitude || ' ' || doctor_profiles.longitude || ')'
      ),
      ST_GeographyFromText('SRID=4326;POINT(%f %f)'),
      %d
    )
  } % [longitude, latitude, distance_in_meters]
  end
end
