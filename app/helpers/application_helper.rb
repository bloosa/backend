module ApplicationHelper
  require "browser/aliases"
  Browser::Base.include(Browser::Aliases)


  # From https://github.com/rwz/nestive/blob/master/lib/nestive/layout_helper.rb
  def extends(layout, &block)
    # Make sure it's a string
    layout = layout.to_s

    # If there's no directory component, presume a plain layout name
    layout = "layouts/#{layout}" unless layout.include?('/')

    # Capture the content to be placed inside the extended layout
    @view_flow.get(:layout).replace capture(&block)

    render file: layout
  end

  def default_page_title
    ENV['WB_DEFAULT_PAGE_TITLE'] || 'Bloosa'
  end

  def is_mobile?
    browser.mobile?
  end


  def render_static(where)
    @page = params[:page]
    @page = @page.gsub(/-/, '_')
    if file_exists_in_views?(where)
      render (where + '/page')
    else
      render file: "public/404.html", status: :not_found
    end
  end

  def render_icon(icon_id)
    render partial: 'partials/icon', :locals => {:icon => icon_id}
  end

  def get_tab_active_class(is_active)
    is_active ? 'is-active' : ''
  end

  def file_exists_in_views?(where)
    file_exists_in_app?("app/views/#{where}/_#{@page}.html.erb")
  end

  def file_exists_in_app?(path)
    File.exist?(Pathname.new(Rails.root + path))
  end
end
