module ProfessionalHelper
  def is_doctor?
    unless current_user&.is_doctor?
      redirect_to doctor_register_as_professional_path
    end
  end

  def idle_profile_status?
    unless current_user&.doctor_profile&.is_confirmed?
      if current_user.doctor_profile.idle_profile_status?
        redirect_to doctor_profile_confirm_request_required_documents_path
      end
    end
  end
  # check if doctor is waiting request
  def waiting_confirmation_status?
    unless current_user&.doctor_profile&.is_confirmed?
      if current_user.doctor_profile.waiting_confirmation_status?
        redirect_to doctor_dashboard_pending_path
      end
    end
  end

  def can_proceed_view_if_not_confirmed
    controller = controller_name
    (controller == 'profile_controller' && action_name == "confirm_profile") || current_user.doctor_profile.is_confirmed
  end

  def set_request_variant
    request.format = 'html' if request.format == 'mobile'
  end
end
