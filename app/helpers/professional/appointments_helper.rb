module Professional::AppointmentsHelper

  def appointments_future_count
    Appointment.where({:doctor_id => current_user.id}).where('time > ?', Time.now).count()
  end

end
