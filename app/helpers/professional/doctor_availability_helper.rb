module Professional::DoctorAvailabilityHelper

  def should_configure_scheduler?
    current_user.doctor_availabilities.count == 0
  end

  def should_configure_system?
    current_user.professional_config.nil?
  end

end
