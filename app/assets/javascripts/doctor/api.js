document.addEventListener("turbolinks:load",
    function () {
        window.PrefessionalApi = {
            /**
             *
             * @param lat
             * @param lng
             * @param cb
             */
            reverseGeo: function (lat, lng, cb) {
                $.get(window.appConfig.baseApi + 'professional/geo/reverse/?lat=' + lat + '&lng=' + lng).then((response) => {
                    cb(response)
                })
            }
        };
    }
);