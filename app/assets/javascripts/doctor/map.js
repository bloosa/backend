document.addEventListener("turbolinks:load", function () {
    // store object related to availabilty
    window.DoctorAvailability = {
        map: null,
        mapModalContainer: null,
        marker: null,
        callbackFn: null,
        initMap: function () {
            var $this = window.DoctorAvailability;
            $this._buildMap();
            $this._initMarker();

        },
        /**
         *
         * @param callbackFn
         */
        showMap: function (callbackFn) {
            var $this = window.DoctorAvailability;
            var el = $this.mapModalContainer;
            if (el) {
                el.classList.add('is-active')
            }
            $this.callbackFn = callbackFn;

        }, /**
         *
         * @param e
         * @private
         */
        _buildMap: function (e) {
            var $this = window.DoctorAvailability;
            var mapModalContainer = document.getElementById('modal_map_container');
            var mapModal = document.getElementById('modal_map');
            if (mapModal && mapModalContainer) {
                mapModal.style.width = '500px';
                mapModal.style.height = '500px';
                var map = new L.map(mapModal, {zoomControl: false});
                //add zoom control with your options
                L.control.zoom({
                    position: 'bottomright'
                }).addTo(map);

                map.setView([51.505, -0.09], 13);
                L.tileLayer(
                    'https://api.mapbox.com/styles/v1/mapbox/emerald-v8/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiaG91c3NlbS1mYXRoYWxsYWgiLCJhIjoiY2prazF5bzRtMGdyaTNycDg4Yjd0YTd1ZiJ9._qOb9j6wrIsWzhFmRNkkxg', {
                        tileSize: 512,
                        zoomOffset: -1,
                        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
                    }).addTo(map);

                map.on('click', $this.onMapClick);

                $this.map = map;
                $this.mapModalContainer = mapModalContainer;

            }
        },
        /**
         *
         * @param e
         * @private
         */
        _initMarker: function (e) {
            var $this = window.DoctorAvailability;
            var map = $this.map;
            var marker = new L.marker(map.getCenter());
            marker.on('dragend', function (event) {
                var marker = event.target;
                var position = marker.getLatLng();
                console.log(position);
                marker.setLatLng(position, {id: uni, draggable: 'true'}).bindPopup(position).update();
            });
            map.addLayer(marker);
            $this.marker = $this;
        },
        /**
         * Callback when map clicked
         * @param e
         */
        onMapClick: function (e) {
            var coord = e.latlng;
            var lat = coord.lat;
            var lng = coord.lng;
            var $this = window.DoctorAvailability;
            if ($this.callbackFn) {
                $this.callbackFn(lat, lng);
            }
            console.log("You clicked the map at latitude: " + lat + " and longitude: " + lng);
        },
        hideMap: function (callbackFn) {
            var el = window.DoctorAvailability.mapModalContainer;
            if (el) {
                el.classList.add('is-active')
            }

        }
    };

    /**
     * Called when the doctor will define it's address
     * Input id, id of the input to fill address
     */
    window.selectAvailabilityAddress = function (event, inputId, previousLocation) {
        // prevent
        event.preventDefault();
        // check configuration
        if (!window.DoctorAvailability.map) {
            window.DoctorAvailability.initMap();
        }
        window.DoctorAvailability.showMap(function (lat, lng) {
            window.PrefessionalApi.reverseGeo(lat, lng, function (response) {

            });
            document.getElementById(inputId).value = [lat, lng].join(',');
        });
        return false
    };
});
