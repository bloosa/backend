(function (A) {
    if (!Array.prototype.forEach)
        A.forEach = A.forEach || function (action, that) {
            for (var i = 0, l = this.length; i < l; i++)
                if (i in this)
                    action.call(that, this[i], i, this);
        };

})(Array.prototype);
(() => {
    var map, marker;

    function updateMap(items) {
        items.forEach(function (item) {
            console.log([item.doctor_profile.latitude, item.doctor_profile.longitude]);
            marker = new L.marker([item.doctor_profile.latitude, item.doctor_profile.longitude]);

            marker.addTo(map);
        });
    };

    function setMap() {
        var
            mapObject,
            markers = [];
        setTimeout(() => {
            let mapElement = document.getElementById('map_listing');
            if (mapElement) {
                let $parentContainer = $(mapElement).parent();
                let width = $parentContainer.width();
                let height = $parentContainer.height();
                mapElement.style.width = width + 'px';
                mapElement.style.height = Math.max(300, height) + 'px';
                map = new L.map(mapElement, {zoomControl: false});
                //add zoom control with your options
                L.control.zoom({
                    position: 'bottomright'
                }).addTo(map);
                map.setView([34.300186, 9.355957], 5);

                L.tileLayer(
                    'https://api.mapbox.com/styles/v1/mapbox/emerald-v8/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiaG91c3NlbS1mYXRoYWxsYWgiLCJhIjoiY2prazF5bzRtMGdyaTNycDg4Yjd0YTd1ZiJ9._qOb9j6wrIsWzhFmRNkkxg', {
                        tileSize: 512,
                        zoomOffset: -1,
                        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
                    }).addTo(map);


                var myIcon = L.icon({
                    iconUrl: 'assets/pins/' + 'Doctors' + '.png',
                    iconSize: [38, 95],
                    iconAnchor: [22, 94],
                    popupAnchor: [-3, -76],
                    shadowSize: [68, 95],
                    shadowAnchor: [22, 94]
                });
            }


        }, 1000);

        function hideAllMarkers() {
            for (var key in markers)
                markers[key].forEach(function (marker) {
                    marker.setMap(null);
                });
        };

        function toggleMarkers(category) {
            hideAllMarkers();

            if ('undefined' === typeof markers[category])
                return false;
            markers[category].forEach(function (marker) {
                marker.setMap(mapObject);
                marker.setAnimation(L.Animation.DROP);

            });
        }

        function onHtmlClick(location_type, key) {
            L.event.trigger(markers[location_type][key], "click");
        }
    }

// main function
    document.addEventListener("turbolinks:load", function () {
        let $el = $('#list_doctors_container');
        if ($el) {
            new Vue({
                el: '#list_doctors_container',
                template: '#list_vue_template',
                data() {
                    return {
                        items: [],
                        isMapShown: true,
                        totalItems: null
                    }
                },
                methods: {
                    toggleMap($event) {
                        console.log('toggle');
                        console.log(this.isMapShown);

                        this.isMapShown = !this.isMapShown
                    }
                },
                mounted() {
                    let uri = window.location.search.substring(1);
                    let urlSearchParams = new URLSearchParams(uri);
                    let params = {};

                    for (var entry of urlSearchParams.entries()) {
                        params[entry[0]] = entry[1]
                    }

                    // get list of item
                    $.get(window.appConfig.baseApi + 'professionals/search', Object.assign({}, params)).then((response) => {
                        this.items = response.data;
                        this.totalItems = response.totalCount;
                        updateMap(this.items);
                    });
                }
            });

            setMap();
        }
    });
})()