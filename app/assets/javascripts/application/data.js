document.addEventListener("turbolinks:load", function () {
    /**
     *
     * @type {string}
     */
    let specialitiesIds = 'specialities';
    if (document.getElementById(specialitiesIds)) {
        let specialities = new Vue({
            el: '#' + specialitiesIds,
            data() {
                return {
                    items: []
                }
            },
            mounted() {
                // get list of item
                $.get(window.appConfig.baseApi + 'specialities/').then((response) => {

                    this.items = response.list.map(entry => {
                        entry.name = entry['en_' + window.appConfig.locale];
                        return entry ;
                    });
                    console.log(    this.items)
                });
            }
        });
    }
    /**
     *
     * @type {string}
     */
    let citiesIds = 'cities';
    if (document.getElementById(citiesIds)) {
        let cities = new Vue({
            el: '#' + citiesIds,
            data() {
                return {
                    items: []
                }
            },
            mounted() {
                // get list of item
                $.get(window.appConfig.baseApi + 'geo/cities/').then((response) => {
                    this.items = response.list;
                });
            }
        });
    }


    /** Carroussel **/
    /**
     *
     * @type {string}
     */
    let recommdedDomId = 'recommended_container';
    let $recommended = document.getElementById(recommdedDomId);
    if ($recommended) {
        new Vue({
            template: '#recommended_vue_template',
            el: '#' + recommdedDomId,
            data() {
                return {
                    items: []
                }
            },
            mounted() {
                // get list of item
                $.get(window.appConfig.baseApi + 'doctors/recommended/').then((response) => {
                    this.items = response.data;
                });
            },
            updated: function () {

            }
        });
    }

});