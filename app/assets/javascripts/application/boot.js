/**
 * Handle contact tabs
 */
function handleContactTabs() {
    $('.tabbed-links li').click(function () {
        let id = $(this).data('contact');
        console.log('#' + id);
        var isActive = "is-active";
        var isHidden = "is-hidden";
        // remove active from all li
        $('.tabbed-links li').removeClass(isActive);
        // hide all contact block
        $('.contact-block').addClass(isHidden);

        // add is-active to current clicked item
        $(this).addClass(isActive);
        // remove is-hidden from contact block
        $('#' + id).removeClass(isHidden);
    })
}


/**
 * Handle animation scroll reveal
 */
function revealOnScroll() {
    /*
    Scrolle top
     */
   /* var win_height_padded = 5;
    var scrolled = $(window).scrollTop();
    win_height_padded = $(window).height() * 1.1
    $("#doctor_features").find('.feature').each(function () {
        var $this = $(this),
            offsetTop = $this.offset().top;
        console.log('%c element', '#f00')
        if (scrolled + win_height_padded > offsetTop) {
            if ($this.data('timeout')) {
                window.setTimeout(function () {
                    $this.addClass('gelatine ' + $this.data('animation'));
                }, parseInt($this.data('timeout'), 30));
            } else {
                $this.addClass('gelatine ' + $this.data('animation'));
            }
        }
    });*/

}

document.addEventListener("turbolinks:load", function () {
    //Preloader
    $(window).on('load', function () { // makes sure the whole site is loaded
        $('#status').fadeOut(); // will first fade out the loading animation
        $('#preloader').fadeOut();// will fade out the white DIV that covers the website.
        $('body').delay(350).css({'overflow': 'visible'});
    });

    //Mobile menu toggle
    if ($('.navbar-burger').length) {
        $('.navbar-burger').on("click", function () {

            var menu_id = $(this).attr('data-target');
            $(this).toggleClass('is-active');
            $("#" + menu_id).toggleClass('is-active');
            $('.navbar.is-light').toggleClass('is-dark-mobile')
        });
    }

    //Animate left hamburger icon and open sidebar
    $('.menu-icon-trigger').click(function (e) {
        e.preventDefault();
        $('.menu-icon-wrapper').toggleClass('open');
        $('.sidebar').toggleClass('is-active');
    });

    //Close sidebar
    $('.sidebar-close').click(function () {
        $('.sidebar').removeClass('is-active');
        $('.menu-icon-wrapper').removeClass('open');
    })

    //Sidebar menu
    if ($('.sidebar').length) {
        $(".sidebar-menu > li.have-children > a").on("click", function (i) {
            i.preventDefault();
            if (!$(this).parent().hasClass("active")) {
                $(".sidebar-menu li ul").slideUp();
                $(this).next().slideToggle();
                $(".sidebar-menu li").removeClass("active");
                $(this).parent().addClass("active");
            }
            else {
                $(this).next().slideToggle();
                $(".sidebar-menu li").removeClass("active");
            }
        });
    }

    //Navbar Clone
    if ($('#navbar-clone').length) {
        $(window).scroll(function () {    // this will work when your window scrolled.
            var height = $(window).scrollTop();  //getting the scrolling height of window
            if (height > 50) {
                $("#navbar-clone").addClass('is-active');
            } else {
                $("#navbar-clone").removeClass('is-active');
            }
        });
    }

    //reveal elements on scroll so animations trigger the right way
    var $window = $(window),
        isTouch = Modernizr.touch;

    $window.on('scroll', revealOnScroll);
    var sr = window.ScrollReveal();
    sr.reveal('#doctor_features .feature', {
        origin: 'bottom',
        distance: '20px',
        duration: 600,
        delay: 100,
        rotate: {
            x: 0,
            y: 0,
            z: 0
        },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: false,
        useDelay: 'always',
        viewFactor: 0.2
    }, 160);
    sr.reveal('.feature-card-middle img', {
        origin: 'top',
        distance: '20px',
        duration: 600,
        delay: 100,
        rotate: {
            x: 0,
            y: 0,
            z: 0
        },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: false,
        useDelay: 'always',
        viewFactor: 0.2
    }, 160); // Revealing features

    // handle contact tabs
    handleContactTabs();

    // Back to Top button behaviour
    var pxShow = 600;
    var scrollSpeed = 500;
    $(window).scroll(function () {
        if ($(window).scrollTop() >= pxShow) {
            $("#backtotop").addClass('visible');
        } else {
            $("#backtotop").removeClass('visible');
        }
    });
    $('#backtotop a').on('click', function () {
        $('views, body').animate({
            scrollTop: 0
        }, scrollSpeed);
        return false;
    });

    // Select all links with hashes
    $('a[href*="#"]')
    // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('views, body').animate({
                        scrollTop: target.offset().top
                    }, 550, function () {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        }
                        ;
                    });
                }
            }
        });
});
