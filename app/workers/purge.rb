#queue = Sidekiq::Queue.new("default")
all = Sidekiq::Cron::Job.all

all.each do |job|
  job.klass # => 'MyWorker'
  job.args # => [1, 2, 3]
  job.destroy
  puts job.klass
end
