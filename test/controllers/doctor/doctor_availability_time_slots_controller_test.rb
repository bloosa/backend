require 'test_helper'

class DoctorAvailabilityTimeSlotsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @doctor_availability_time_slot = doctor_availability(:one)
  end

  test "should get index" do
    get doctor_availability_url
    assert_response :success
  end

  test "should get new" do
    get new_doctor_availability_time_slot_url
    assert_response :success
  end

  test "should create doctor_availability_time_slot" do
    assert_difference('DoctorAvailabilityTimeSlot.count') do
      post doctor_availability_url, params: { doctor_availability_time_slot: {  } }
    end

    assert_redirected_to doctor_availability_time_slot_url(DoctorAvailability.last)
  end

  test "should show doctor_availability_time_slot" do
    get doctor_availability_time_slot_url(@doctor_availability_time_slot)
    assert_response :success
  end

  test "should get edit" do
    get edit_doctor_availability_time_slot_url(@doctor_availability_time_slot)
    assert_response :success
  end

  test "should update doctor_availability_time_slot" do
    patch doctor_availability_time_slot_url(@doctor_availability_time_slot), params: { doctor_availability_time_slot: {  } }
    assert_redirected_to doctor_availability_time_slot_url(@doctor_availability_time_slot)
  end

  test "should destroy doctor_availability_time_slot" do
    assert_difference('DoctorAvailabilityTimeSlot.count', -1) do
      delete doctor_availability_time_slot_url(@doctor_availability_time_slot)
    end

    assert_redirected_to doctor_availability_url
  end
end
