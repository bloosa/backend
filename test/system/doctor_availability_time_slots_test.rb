require "application_system_test_case"

class DoctorAvailabilityTimeSlotsTest < ApplicationSystemTestCase
  setup do
    @doctor_availability_time_slot = doctor_availability(:one)
  end

  test "visiting the index" do
    visit doctor_availability_url
    assert_selector "h1", text: "Doctor Availability Time Slots"
  end

  test "creating a Doctor availability time slot" do
    visit doctor_availability_url
    click_on "New Doctor Availability Time Slot"

    click_on "Create Doctor availability time slot"

    assert_text "Doctor availability time slot was successfully created"
    click_on "Back"
  end

  test "updating a Doctor availability time slot" do
    visit doctor_availability_url
    click_on "Edit", match: :first

    click_on "Update Doctor availability time slot"

    assert_text "Doctor availability time slot was successfully updated"
    click_on "Back"
  end

  test "destroying a Doctor availability time slot" do
    visit doctor_availability_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doctor availability time slot was successfully destroyed"
  end
end
